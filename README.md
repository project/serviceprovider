<!-- writeme -->
Serviceprovider
===============

Provides a service provider role and directly related functionality.

 * https://gitlab.com/find-it-program-locator/serviceprovider
 * Issues: https://gitlab.com/find-it-program-locator/serviceprovider/issues
 * Source code: https://gitlab.com/find-it-program-locator/serviceprovider/tree/8.x-1.x
 * Keywords: find it, drupal, drutopia, config, organization, service provider
 * Package name: drupal/serviceprovider


### Requirements

No dependencies.


### License

GPL-2.0+

<!-- endwriteme -->

### Background

Originally developed for the [Find It Program Locator platform](https://gitlab.com/find-it-program-locator ).

### A micro-configuration feature

By itself, this module does little to nothing, and that is by design. It exists to make it easier for independent modules providing functionality in the service provider space to be compatible with one another.

The rationale behind role-only modules (and default roles in Drupal core) is to allow contributed modules (and core modules) to set intelligent defaults, just as many modules do for anonymous and authenticated roles.
